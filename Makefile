screenshot: screenshot.cc
	g++ -Wall -Werror -DUSE_GLOG \
	  -o screenshot screenshot.cc \
	  `pkg-config --cflags --libs cairo gflags libglog x11`

all: screenshot

clean:
	rm -f screenshot
